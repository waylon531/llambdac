//    A toy compiler for a lambda-calculus language
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#![feature(box_syntax,box_patterns)]
extern crate llvm_sys;
extern crate rand;
mod ast;
mod llambda;
#[cfg(test)]
mod test;
use llvm_sys::{core,analysis,execution_engine,target};
use std::ffi::CString;
use std::io;
use std::io::BufRead;
use std::mem;
fn main() {
    let mut input = String::new();
    {
    let stdin = io::stdin();
    let mut handle = stdin.lock();
    handle.read_line(&mut input).expect("Bad input?");
    }
    let result: f64 = unsafe { get_result(input.clone()) };
    println!("{}", result);
}
pub unsafe fn get_result(input: String) -> f64 {
    //Builder getting set up
    let module = core::LLVMModuleCreateWithName(CString::new("Global").unwrap().as_ptr());
    let builder = core::LLVMCreateBuilder();
    let ret_type = core::LLVMFunctionType(core::LLVMDoubleType(), core::LLVMVoidType() as *mut *mut llvm_sys::LLVMType, 0, 0);
    let main = core::LLVMAddFunction(module,CString::new("main").unwrap().as_ptr(),ret_type);
    let entry = core::LLVMAppendBasicBlock(main, CString::new("entry").unwrap().as_ptr());
    core::LLVMPositionBuilderAtEnd(builder, entry);
    //Builder now setup
    #[cfg(feature = "debug")]
    println!("Building AST");
    let ast = box llambda::parse_Expr(input.as_str()).unwrap();
    #[cfg(feature = "debug")]
    println!("AST: {:?}",ast);
    #[cfg(feature = "debug")]
    println!("Built AST");
    #[cfg(feature = "debug")]
    println!("Generating code");
    ast.generate_code(builder);
    #[cfg(feature = "debug")]
    println!("Generated code");

    core::LLVMDisposeBuilder(builder);

    let error = CString::new(Vec::with_capacity(512)).unwrap();
    analysis::LLVMVerifyModule(module, llvm_sys::analysis::LLVMVerifierFailureAction::LLVMAbortProcessAction, error.as_ptr() as *mut *mut i8);
    //println!("Error message: {:?}",error);
    core::LLVMDisposeMessage(error.as_ptr() as *mut i8);

    #[cfg(feature = "debug")]
    println!("Building engine");
	// build an execution engine
    execution_engine::LLVMLinkInMCJIT();
	target::LLVM_InitializeNativeTarget();
    target::LLVM_InitializeNativeAsmPrinter();
	let mut ee = mem::uninitialized();
    let mut out = mem::zeroed();
    #[cfg(feature = "debug")]
    println!("Spinning up engine");
    execution_engine::LLVMCreateExecutionEngineForModule(&mut ee, module, &mut out);
    //println!("ENGINE {:?}",ee);

    #[cfg(feature = "debug")]
    println!("Conjuring value");
	let value = execution_engine::LLVMRunFunction(ee, main, 0, core::LLVMVoidType() as *mut *mut llvm_sys::execution_engine::LLVMOpaqueGenericValue);
    let final_float = execution_engine::LLVMGenericValueToFloat(core::LLVMDoubleType(),value) as f64;
    execution_engine::LLVMDisposeExecutionEngine(ee);
    return final_float;
}
