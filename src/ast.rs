//    A toy compiler for a lambda-calculus language
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


use llvm_sys::{core,prelude};
use std::ffi::CString;
use std::collections::HashMap;
use rand;
//Things that give you a result
#[derive(PartialEq,Debug,Clone)]
pub enum Expr {
    Number(f64),
    Func(Func),
    Var(String), //Variable name
    //Assign(String,Box<Expr>), //var name, thing to assign
    Apply(Box<Expr>,Vec<Box<Expr>>), //function, arguments
    Loop(usize,Vec<(String,Box<Expr>)>,Box<Expr>) //Number of loops, vec of lets, next expression
}
impl Expr {
    //HOW TO PARSE
    pub unsafe fn generate_code(&self, builder: prelude::LLVMBuilderRef) {
        //help
        //go to bottom and build up?
        //recursive something
        //Maybe somehow do stuff without inlining
        core::LLVMBuildRet(builder,self.traverse(builder,&mut HashMap::new()));

    }
    //This needed more forethought
    //Is now a clusterfuck of clones
    pub unsafe fn traverse(&self,builder: prelude::LLVMBuilderRef,mut values: &mut HashMap<String,Box<Expr>>) -> prelude::LLVMValueRef {
        //std::intrinsics::breakpoint();
        //#[cfg(feature="debug")]
        //println!("Traversing: {:?}",self);
        match self {
            &Expr::Number(x) => {
                core::LLVMConstReal(core::LLVMDoubleType(),x)
            },
            &Expr::Var(ref v) => {
                if values.contains_key(v) {
                    let temp_value =  {
                        *values.get(v).unwrap().clone()
                    };
                    temp_value.clone().traverse(builder,values)
                } else {
					panic!("Variable {} has no value",v)
                }
            },
            &Expr::Loop(num,ref lets,ref next) => {
                match num {
                    //finish loop
                    0 => {
                        next.traverse(builder,values)
                    },
                    _ => {
                        //TODO: maybe I can remove the clone of next?
                        let mut final_expr = box Expr::Loop(num-1,lets.clone(),next.clone());
                        for l in lets.iter().rev() {
                            match l {
                            &(ref var,ref expr) => final_expr = Box::new(Expr::Apply(Box::new(Expr::Func(Func::Lambda(vec![var.clone()],final_expr))),vec![expr.clone()]))
                            }
                        }
                        final_expr.traverse(builder,values)
                    }
                }
            },
            &Expr::Func(ref f) => {
                match f {
                    &Func::Rand=> 
                    core::LLVMConstReal(core::LLVMDoubleType(),rand::random::<f64>()),
                    _ => panic!("Cannot return funcs: {:?}",f)
                }
            },
            &Expr::Apply(ref func,ref args) => {
                match **func {
                    Expr::Func(Func::Mul) => core::LLVMBuildFMul(builder, args[0].traverse(builder,values), args[1].traverse(builder,values), CString::new("tmpmul").unwrap().as_ptr()),
                    Expr::Func(Func::Div) => core::LLVMBuildFDiv(builder, args[0].traverse(builder,values), args[1].traverse(builder,values), CString::new("tmpdiv").unwrap().as_ptr()),
                    Expr::Func(Func::Add) => core::LLVMBuildFAdd(builder, args[0].traverse(builder,values), args[1].traverse(builder,values), CString::new("tmpadd").unwrap().as_ptr()),
                    Expr::Func(Func::Sub)=> core::LLVMBuildFSub(builder, args[0].traverse(builder,values), args[1].traverse(builder,values), CString::new("tmpsub").unwrap().as_ptr()),
                    Expr::Func(Func::Mod)=> core::LLVMBuildFRem(builder, args[0].traverse(builder,values), args[1].traverse(builder,values), CString::new("tmprem").unwrap().as_ptr()),
                    /*&mut box Expr::Func(Func::Abs)=> 
                        core::LLVMBuildOr(builder, 
                            args[0].traverse(builder,values), 
                            core::LLVMConstInt(core::LLVMInt64Type(),0x8000000000000000 as u64,0),
                            CString::new("tmprem").unwrap().as_ptr()),*/
                    Expr::Func(Func::Rand)=> core::LLVMConstReal(core::LLVMDoubleType(),rand::random::<f64>()),
                    Expr::Func(Func::Lambda(ref vars,ref expr)) => {
                        for i in 0..vars.len() {
                            let arg = args[i].clone().reduce(values);
                            values.insert(vars[i].clone(),arg);
                        }
                        //RIP ME infinite recursion so fun
                        //reduce args before traversing?
                        //can I even reduce the args?
                        //I prob should have made this an interpreter
                        //I guess I can substitute in variables wherever there are any
                        expr.traverse(builder,values)
                    },
                    //TODO: save as function
                    //right now this is just an alias
                    Expr::Var(ref name) => {
                        Expr::Apply( values.get_mut(name).expect("Function does not exist").clone(),args.clone()).traverse(builder,values) 
                    },
                    ref e @ _ => {
                        panic!("Invalid apply: {:?}",e);
                    }
                }
            }
            
        }
    }
    fn reduce(&self,values: &HashMap<String,Box<Expr>>) -> Box<Expr>{
        //find free Expr::Var and replace with value
        //don't replace the x in \x.
        //PROBABLY
        //or stuff in lambdas
        //safe to do in apply.args
        match self {
            &Expr::Var(ref name) => {
                match values.get(name) {
                    Some(value) => value.clone(),
                    None => panic!("Variable {} has nothing assigned",name)
                }
            },
            &Expr::Apply(ref func,ref vars) => {
                let mut new_vars = Vec::new();
                for v in vars {
                    new_vars.push(v.reduce(values));
                }
                box Expr::Apply(func.clone(),new_vars)
            }
            _ => box self.clone()
        }

    }
}
#[derive(PartialEq,Debug,Clone)]
pub enum Func {
    Add,
    Sub,
    Div,
    Mul,
    Mod,
	Rand,
    //Abs,
    Lambda(Vec<String>,Box<Expr>),
    //Named(String)
}

