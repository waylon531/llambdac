//    A toy compiler for a lambda-calculus language
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


use std::str::FromStr;
use ast::{Expr,Func};

grammar;

pub Expr: Box<Expr> = {
    Num => Box::new(Expr::Number(<>)),
    "(" <Expr> ")",
    "(" <f:Expr> <e:(Expr)+> ")" => Box::new(Expr::Apply(f,e)),
    Func => Box::new(Expr::Func(<>)),
    Let,
    Var => Box::new(Expr::Var(<>.to_owned())),
    //How do I expand loops?
    //Maybe don't unroll all loops
    "loop" <n:Num> "{" <l:(LoopLet)+> "}" <next:Expr> => {
        Box::new(Expr::Loop(n as usize,l,next))
        //let mut final_expr = next;
        //for _ in 0..n as usize {
        //    for le in l.iter().rev() {
        //        match le {
        //            &util::TmpLoop(ref var,ref expr) =>
        //            {final_expr = Box::new(Expr::Apply(Box::new(Expr::Func(Func::Lambda(vec![var.clone()],final_expr))),vec![expr.clone()]));
        //            }

        //        }
        //    }
        //}
        //final_expr
    }
};

Let: Box<Expr> = {
    "let" <v:Var> "=" <e:Expr> ";" <next:Expr> => Box::new(Expr::Apply(Box::new(Expr::Func(Func::Lambda(vec![v],next))),vec![e])),
};

LoopLet: (String,Box<Expr>) = {
    "let" <v:Var> "=" <e:Expr> ";" => (v,e),
    "let" <v:Var> "=" <e:Expr>  => (v,e),
};

Func: Func = {
    "*" => Func::Mul,
    "/" => Func::Div,
    "+" => Func::Add,
    "-" => Func::Sub,
    "%" => Func::Mod,
    "rand" => Func::Rand,
    //"abs" => Func::Abs,
    //"sin" => Func::Sin,
    r"\\" <v:Var+> r"\." <e:Expr> => Func::Lambda(v,e),
    //Var => Func::Named(<>)
};

Num: f64 = <s:r"-?[0-9]*(\.[0-9]+)?"> => f64::from_str(s).unwrap();
//Int: u64 = <s:r"[0-9]+"> => u64::from_str(s).unwrap();
Var: String = <s:r"[a-zA-Z_\p{Katakana}]+"> => s.to_owned();

//Program full of statements
//Statement 

// vim: ft=rust
