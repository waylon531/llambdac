#[cfg(test)]
mod test {
    mod ast { 
        use ast::{Func,Expr};
        use llambda;
        #[test]
        fn basic_test() {
            assert_eq!(llambda::parse_Expr("( + 2 1 )").unwrap(),box Expr::Apply(box Expr::Func(Func::Add),vec![box Expr::Number(2f64), box Expr::Number(1f64)]));
        }
        #[test]
        fn less_basic_test() {
            assert_eq!(llambda::parse_Expr("( * ( + 2 1 ) 3 )").unwrap(),box Expr::Apply(box Expr::Func(Func::Mul),vec![box Expr::Apply(box Expr::Func(Func::Add),vec![box Expr::Number(2f64), box Expr::Number(1f64)]),box Expr::Number(3f64)]));
        }
        #[test]
        fn lambda_test() {
            assert_eq!(llambda::parse_Expr(r"( \x.(+ x 1) 1 )").unwrap(),box Expr::Apply(box Expr::Func(Func::Lambda(vec!["x".to_owned()],box Expr::Apply(box Expr::Func(Func::Add),vec![box Expr::Var("x".to_owned()),box Expr::Number(1f64)]))),vec![box Expr::Number(1f64)]));
        }
        #[test]
        fn let_test() {
            assert_eq!(llambda::parse_Expr(r"let x = 1; x").unwrap(),box Expr::Apply(box Expr::Func(Func::Lambda(vec!["x".to_owned()],box Expr::Var("x".to_owned()))),vec![box Expr::Number(1f64)]));
        }
        #[test]
        fn let_func_test() {
            assert_eq!(llambda::parse_Expr(r"let x = *; x").unwrap(),box Expr::Apply(box Expr::Func(Func::Lambda(vec!["x".to_owned()],box Expr::Var("x".to_owned()))),vec![box Expr::Func(Func::Mul)]));
        }
    }
    mod result {
        use super::super::super::get_result;
       #[test]
        fn loop_test() {
            unsafe {
            assert_eq!(8f64,get_result("let x = 1; loop 3 { let x = ( * x 2 ) } x".to_owned()));
            }
        }
       #[test]
        fn katakana_test() {
            unsafe {
            assert_eq!(1f64,get_result("let ツ = 1; ツ".to_owned()));
            }
        }
       #[test]
        fn two_thing_test() {
            unsafe {
            assert_eq!(4f64,get_result(r"let two_thing = \x.(x 2 2); (two_thing * )".to_owned()));
            }
        }
       #[test]
        fn forty_two_test() {
            unsafe {
            assert_eq!(42f64,get_result(r"let seven = (+ 4 3); let square = \x.(* x x); ( - (square seven) seven )".to_owned()));
            }
        }
       #[test]
        fn expand_loop_test() {
            unsafe {
            assert_eq!(1024f64*1024f64,get_result("let x = 1; loop 20 { let x = ( + x x ) } x".to_owned()));
            }
        }
       #[test]
        fn big_loop_test() {
            unsafe {
            assert_eq!(100f64,get_result("let x = 0; loop 100 { let x = ( + x 1 ) } x".to_owned()));
            }
        }
       #[test]
        fn multi_loop_test() {
            unsafe {
            assert_eq!(30f64,get_result("let x = 0; loop 4 { let x = ( + x 1 ) let x = ( * x 2 ) } x".to_owned()));
            }
        }

    }
}
